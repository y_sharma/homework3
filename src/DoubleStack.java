import java.util.LinkedList;

public class DoubleStack {
	// Realiseerida abstraktne andmet��p "reaalarvude magasin" (LIFO) ahela
	// (linkedlist) abil. Magasini s�nena esitamisel olgu tipp l�pus (meetod
	// toString() v�ljastab elemendid p�hja poolt tipu poole).

	// Kommentaar: Hilinemine -60%. Meetod op peaks kontrollima, et magasinis on
	// v�hemalt kaks arvu ning et tehtem�rk on lubatud. Meetod interpret ei �tle
	// vea korral, milline avaldis oli vigane (neid v�ib ju olla palju).

	public static void main(String[] argum) {
		// TODO!!! Your tests here!
		DoubleStack m = new DoubleStack();
		System.out.println(m);
		m.push(1);
		System.out.println(m);
		m.push(2);
		System.out.println(m);
		m.push(3);
		System.out.println(m);
		m.push(4);

		System.out.println("enne op tehet: " + m);
		m.op("+");
		System.out.println("p�rast op tehet: " + m);

		DoubleStack koopia = new DoubleStack();
		try {
			koopia = (DoubleStack) m.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("vana: " + m);
		System.out.println("clone: " + koopia);

		System.out.println("interpret meetod: " + interpret("1 2 uu"));

	}

	private LinkedList<Double> magasin;

	// uue magasini konstruktor
	DoubleStack() {
		magasin = new LinkedList<Double>();
	}

	// koopia loomine
	@Override
	public Object clone() throws CloneNotSupportedException {
		DoubleStack uus = new DoubleStack();

		// kui vana magasin ei ole t�hi
		if (!stEmpty()) {
			// t�idab uue magasini vana magasini andmetega
			for (int i = 0; i < magasin.size(); i++) {
				// push millegip�rast ei t��ta, peab kasutama addLast
				uus.magasin.addLast(magasin.get(i));
			}
		}

		return uus;
	}

	// kontroll, kas magasin on t�hi
	public boolean stEmpty() {
		if (magasin.isEmpty()) {
			return true;
		}
		return false;
	}

	// magasini elemendi lisamine (l�ppu)
	public void push(double a) {
		magasin.addLast(a);
	}

	// magasinist elemendi v�tmine (l�pust)
	public double pop() {
		// tegelikult viskab removeLast meetod ise ka
		// erindi NoSuchElementException, kui magasin on t�hi
		if (stEmpty()) {
			throw new RuntimeException("ei saa magasinist elementi v�tta, see on t�hi");
		}
		return magasin.removeLast();
	}

	// aritmeetikatehe s ( + - * / ) magasini kahe pealmise elemendi vahel
	// (tulemus pannakse uueks tipuks)
	// * Parandatud: Meetod op peaks kontrollima, et magasinis on
	// v�hemalt kaks arvu ning et tehtem�rk on lubatud
	public void op(String s) {
		// kui magasin on 2 4 6 8, siis j�rjekord on selline
		// et n�iteks jagamine on 6 / 8

		if (magasin.size() < 2) {
			throw new RuntimeException("magasinis ei ole piisavalt arve op meetodi jaoks");
		}
		
		double op2 = pop();
		double op1 = pop();
		if (s.equals("+")) {
			push(op1 + op2);
			return;
		}
		if (s.equals("-")) {
			push(op1 - op2);
			return;
		}
		if (s.equals("*")) {
			push(op1 * op2);
			return;
		}
		if (s.equals("/")) {
			push(op1 / op2);
			return;
		} else {
			throw new RuntimeException("see ei ole lubatud tehtem�rk: " + s);
		}

	}

	// tipu lugemine eemaldamiseta
	public double tos() {
		// tegelikult viskab getLast meetod ise ka
		// erindi NoSuchElementException, kui magasin on t�hi
		if (stEmpty()) {
			throw new RuntimeException("ei saa magasini tippu lugeda, see on t�hi");
		}
		return magasin.getLast();
	}

	// kahe magasini v�rdsuse kindlakstegemine
	@Override
	public boolean equals(Object o) {
		if (magasin.equals(((DoubleStack) o).magasin)) {
			return true;
		}
		return false;
	}

	// teisendus s�neks (tipp l�pus)
	@Override
	public String toString() {
		if (stEmpty()) {
			return "Magasin on t�hi";
		}

		StringBuffer sb = new StringBuffer();
		// k�ib �kshaaval k�ik arvud magasinist l�bi
		for (Double double1 : magasin) {
			// lisab stringbuffer-isse arvu ja t�hiku
			sb.append(String.valueOf(double1));
			sb.append(" ");
		}
		return sb.toString();
	}

	// aritmeetilise avaldise p��ratud poola kuju v�ljaarvutamine
	// * Parandatud: Meetod interpret ei �tle vea korral, milline avaldis oli
	// vigane (neid v�ib ju olla palju)
	public static double interpret(String pol) {
		DoubleStack mag = new DoubleStack();

		// eemaldab esimesed t�hikud
		String tekst = pol.trim();

		// "\\s+" s on t�hik. kui + l�pus, siis arvestab j�rjest k�iki t�hikuid
		for (String tykid : tekst.split("\\s+")) {
			// kui arvutusm�rk, siis proovib tehet teha
			if (tykid.equals("-") || tykid.equals("+") || tykid.equals("/") || tykid.equals("*")) {
				try {
					mag.op(tykid);
				} catch (Exception e) {
					throw new RuntimeException("see avaldis ei ole korrektne: " + pol);
				}
			} else { // kui ei olnud arvutusm�rk, siis peaks olema double
				// proovib parse-da arvu double
				try {
					Double arv = Double.parseDouble(tykid);
					// lisab selle arvu magasii
					mag.push(arv);
				} catch (Exception e) {
					throw new RuntimeException(
							"avaldises on t�hem�rk, mis ei ole korrektne arv ega arvutusm�rk. t�hem�rk: " + tykid
									+ " avaldis: " + pol);
				}
			}
		}
		// kui magasini j��b �leliigseid elemente
		if (mag.magasin.size() > 1) {
			throw new RuntimeException("magasini j�i �leliigseid arve, avaldis: " + pol);
		}

		return mag.tos();

	}

}
